import React from 'react';
import { Redirect, Route } from "react-router-dom";
import { useAuth } from "../contexts/authContext";
import PropTypes from "prop-types";

const PrivateRoute = ({ component, ...rest }) => {
    let auth = useAuth();

    if (auth?.isLogin) {
        const roleRoute = {...rest}?.role;
        const permissionRoute = {...rest}?.permission;
        if (auth.authorize.hasRole(roleRoute) || auth.authorize.hasPermission(permissionRoute)) {
            return (
                <Route {...rest} component={component}/>
            )
        } else {
            return (
                <Redirect to='/forbidden' />
            )
        }
    } else {
        return (
            <Redirect to='/login' />
        )
    }
}

PrivateRoute.propTypes = {
    component: PropTypes.array.isRequired,
};

export default PrivateRoute;
