import Login from "../pages/auth/Login";
import User from "../pages/users/User";
import CreateUser from "../pages/users/CreateUser";
import EditUser from "../pages/users/EditUser";
import ShowUser from "../pages/users/ShowUser";
import Role from "../pages/roles/Role";
import CreateRole from "../pages/roles/CreateRole";
import EditRole from "../pages/roles/EditRole";
import ShowRole from "../pages/roles/ShowRole";
import ForBidden from "../pages/ForBidden";

const routes = [
    {
        path: '/login',
        component: Login,
        is_protected:false
    },
    {
        path: '/forbidden',
        component: ForBidden,
        is_protected: false
    },
    {
        path: '/users',
        component: User,
        is_protected:true,
        role: ['super-admin', 'admin', 'user'],
        permission: 'user-list'
    },
    {
        path: '/users/create',
        component: CreateUser,
        is_protected:true,
        role: ['super-admin', 'admin', 'user'],
        permission: 'user-create'
    },
    {
        path: '/users/update/:id',
        component: EditUser,
        is_protected:true,
        role: ['super-admin', 'admin'],
        permission: 'user-edit'
    },
    {
        path: '/users/show/:id',
        component: ShowUser,
        is_protected:true,
        role: ['super-admin', 'admin', 'user'],
        permission: 'user-list'
    },
    {
        path: '/roles',
        component: Role,
        is_protected:true,
        role: ['super-admin', 'admin', 'user'],
        permission: 'role-list'
    },
    {
        path: '/roles/create',
        component: CreateRole,
        is_protected:true,
        role: ['super-admin', 'admin'],
        permission: 'role-create'
    },
    {
        path: '/roles/update/:id',
        component: EditRole,
        is_protected:true,
        role: ['super-admin', 'admin'],
        permission: 'role-edit'
    },
    {
        path: '/roles/show/:id',
        component: ShowRole,
        is_protected:true,
        role: ['super-admin', 'admin', 'user'],
        permission: 'role-list'
    }
]

export default routes;
