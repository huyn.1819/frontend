import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AuthProvider } from "./contexts/authContext";
import routes from "./routes/routes";
import PrivateRoute from './routes/Protected';

function App() {
    return (
        <AuthProvider>
            <Router>
                <Switch>
                    {routes.map(function (route) {
                        if (route.is_protected) {
                            return (
                                <PrivateRoute exact path={route.path} component={route.component} role={route?.role}
                                              permission={route?.permission}/>
                            )

                        } else {
                            return (
                                <Route exact path={route.path} component={route.component}/>
                            )

                        }
                    })}
                </Switch>
            </Router>
        </AuthProvider>
    );
}

export default App;
