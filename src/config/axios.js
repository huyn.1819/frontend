import axios from 'axios';

const UNAUTHORIZED = 401;

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL_BACKEND,
});

axiosInstance.interceptors.request.use((request) => {
    const token = JSON.parse(localStorage.getItem('access_token')) || '';
    if(token){
        request.headers.Authorization = `Bearer ${token}`;
    }
    return request;
})

axiosInstance.interceptors.response.use(
    response => response,
    ({response}) => {
        if (response.status === UNAUTHORIZED) {
            localStorage.removeItem('access_token');
            localStorage.removeItem('user_current');
            window.location('/login');
        }
        return  Promise.reject(response);

    }
)

export default axiosInstance;