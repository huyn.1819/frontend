import {useCallback} from 'react';
import _ from 'lodash';

export const useDebounce = (func, timeout  = 1000)=> useCallback(_.debounce(func, timeout ), []);