import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import roleService from "../services/roleService";

const initialState = {
    roles: [],
    errors: [],
    role: {},
};

const ERROR_UNPROCESSABLE_ENTITY = 422;

export const getAll = createAsyncThunk(
    "roles/getAll",
    async (data) => {
        return await roleService.getAllRoles(data);
    }
)

export const createRole = createAsyncThunk(
    "roles/create",
    async ( data ) => {
        try {
            return await roleService.createRole(data);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const getRole = createAsyncThunk(
    "roles/getRole",
    async ( id ) => {
        try {
            return await roleService.getRoleById(id);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const updateRole = createAsyncThunk(
    "roles/update",
    async ( {id, data} ) => {
        try {
            return await roleService.updateRole(id, data);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const deleteRole = createAsyncThunk(
    'roles/delete',
    async ( id ) => {
        return await roleService.destroyRole(id);
    }
)

const roleSlice = createSlice( {
    name: "roles",
    initialState,
    extraReducers: {
        [getAll.fulfilled]: (state, action) => {
            state.roles = action.payload.data.data
        },
        [getRole.fulfilled]: (state, action) => {
            state.role = action.payload.data.data
        },
        [createRole.fulfilled]: (state, action) => {
            if (action.payload.status === ERROR_UNPROCESSABLE_ENTITY) {
                state.errors = action.payload.data.error;
            } else {
                state.errors = [];
            }
        },
        [createRole.rejected]: () => {

        },
        [updateRole.fulfilled]: () => {

        },
        [deleteRole.fulfilled]: () => {

        },
    },
});

export const getRoles = state => {
    return state.roles.roles;
}

export const getRoleById = state => {
    return state.roles.role;
}

export const getErrors = state => {
    return state.roles.errors;
}

const { reducer } = roleSlice;
export default reducer;