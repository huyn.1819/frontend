import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import userService from "../services/userService";

const initialState = {
    users: [],
    errors: [],
    user: {},
};

const ERROR_UNPROCESSABLE_ENTITY = 422;

export const getAll = createAsyncThunk(
    "users/getAll",
    async (data) => {
        return await userService.getAllUsers(data);
    }
)

export const createUser = createAsyncThunk(
    "users/create",
    async ( data ) => {
        try {
            return await userService.createUser(data);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const getUser = createAsyncThunk(
    "users/getUser",
    async ( id ) => {
        try {
            return await userService.getUserById(id);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const updateUser = createAsyncThunk(
    "users/update",
    async ( {id, data} ) => {
        try {
            return await userService.updateUser(id, data);
        } catch ( {response} ) {
            return response;
        }
    }
)

export const deleteUser = createAsyncThunk(
    'users/delete',
    async ( id ) => {
        return await userService.destroyUser(id);
    }
)

const userSlice = createSlice( {
    name: "users",
    initialState,
    extraReducers: {
        [getAll.fulfilled]: (state, action) => {
            state.users = action.payload.data.data
        },
        [getUser.fulfilled]: (state, action) => {
            state.user = action.payload.data.data
        },
        [createUser.fulfilled]: (state, action) => {
            if (action.payload.status === ERROR_UNPROCESSABLE_ENTITY) {
                state.errors = action.payload.data.error;
            } else {
                state.errors = [];
            }
        },
        [createUser.rejected]: () => {

        },
        [updateUser.fulfilled]: () => {

        },
        [deleteUser.fulfilled]: () => {

        },
    },
});

export const getUsers = state => {
    return state.users.users;
}

export const getDetailUser = state => {
    return state.users.user;
}

export const getErrors = state => {
    return state.users.errors;
}

const { reducer } = userSlice;
export default reducer;