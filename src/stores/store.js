import roleReducer from "../features/roleSlice";
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import userReducer from "../features/userSlice";

const reducer = combineReducers({
    roles: roleReducer,
    users: userReducer,
})

const store = configureStore({
    reducer: reducer,
    devTools: true,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    }),
})

export default store;

