export default class AuthorizeService {

    constructor(userCurrent) {
        this.roles = userCurrent.roles;
        this.permissions = userCurrent.permissions;
    }

    hasPermission(permission) {
        return this.isSuperAdmin() || this.permissions?.includes(permission) || permission === '';
    }

    hasRole(role) {
        return this.roles?.includes(role) || this.isSuperAdmin() || role === '';
    }

    hasRolePermission(role, permission) {
        return (this.hasRole(role) && this.hasPermission(permission)) || this.isSuperAdmin();
    }

    isSuperAdmin() {
        return this.roles?.includes('super-admin');
    }
}