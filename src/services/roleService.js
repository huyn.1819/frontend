import axiosInstance from "../config/axios";

const getAllRoles = (data = {}) => {
    const queryString = new URLSearchParams(data).toString();
    return axiosInstance.get(`roles?${queryString}`)
};

const getRoleById = id => {
    return axiosInstance.get(`roles/show/${id}`)
};

const createRole = data => {
    return axiosInstance.post('roles', data)
};

const updateRole = (id, data) => {
    return axiosInstance.put(`roles/${id}`, data)
}

const destroyRole = id => {
    return axiosInstance.delete(`roles/${id}`)
}

const roleService = {
    getAllRoles,
    getRoleById,
    createRole,
    updateRole,
    destroyRole
}

export default roleService;