import axiosInstance from "../config/axios";

const Login = (data) => {
    return axiosInstance.post('login', data)
}

export { Login }