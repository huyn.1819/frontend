import axiosInstance from "../config/axios";

const getAllUsers = (data = {}) => {
    const queryString = new URLSearchParams(data).toString();
    return axiosInstance.get(`users?${queryString}`)
};

const getUserById = id => {
    return axiosInstance.get(`users/show/${id}`)
};

const createUser = data => {
    return axiosInstance.post('users', data)
};

const updateUser = (id, data) => {
    return axiosInstance.put(`users/${id}`, data)
}

const destroyUser = id => {
    return axiosInstance.delete(`users/${id}`)
}

const userService = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    destroyUser,
}

export default userService;