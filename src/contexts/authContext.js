import React, { useContext, useState } from "react";
import {Login } from "../services/authService";
import AuthorizeService from "../services/AuthorizeService";
import PropTypes from "prop-types";
import swal from "sweetalert";

const AuthContext = React.createContext(null);

export function AuthProvider ({ children }) {

    const [isLogin, setIsLogin] = useState(false);
    const [userCurrent, setUserCurrent] = useState({})

    const login = async (data) => {
        try {
            let response = await Login(data)
            if (response.data.data.access_token)
            {
                const user = response.data.data;
                localStorage.setItem("user_current", JSON.stringify(user));
                localStorage.setItem("access_token", JSON.stringify(user.access_token));
                setIsLogin(true)
                setUserCurrent(response.data.data.user)
            }
        } catch (error) {
            swal("Oops...", "Something went wrong!", "error");
        }

    }

    const logout = async () => {
        if (localStorage.getItem("user_current") !== null)
        {
            localStorage.removeItem('user_current');
            localStorage.removeItem('access_token');
            setIsLogin(false)
            swal("Successfully!", "You logout successfully!", "success");

        } else {
            swal("Oops...", "You are not login", "error");
        }

    }

    const authenticate = {
        isLogin,
        userCurrent,
        login,
        logout,
        authorize: new AuthorizeService(userCurrent)
    }
    return (
        <AuthContext.Provider value={authenticate}>
            {children}
        </AuthContext.Provider>
    );
}

AuthProvider.propTypes = {
    children: PropTypes.any,
};

export const useAuth = ()=>{
     return useContext(AuthContext);
}
