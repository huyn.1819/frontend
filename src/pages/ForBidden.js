import React from 'react';

const ForBidden = () => {
    return (
        <div align="center">
            <h1>Forbidden!</h1>
            <h3>Code 403</h3>
        </div>

    )
}

export default ForBidden;