import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useState, useEffect} from "react";
import Container from "@mui/material/Container";
import {CssBaseline, FormControl, MenuItem, Select, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {ThemeProvider} from "@emotion/react";
import {createTheme} from "@mui/material/styles";
import {useHistory, useParams} from "react-router";
import {getAll, getDetailUser, getErrors, getUser, updateUser} from "../../features/userSlice";
import swal from "sweetalert";
import Link from "@mui/material/Link";

const STATUS_SUCCESS = 200;

const EditUser = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [role, setRole] = useState("");

   const userEdit = useSelector(getDetailUser);

   const errors = useSelector(getErrors);

   const { id } = useParams();

   const getError = (name) => {
       return errors?.[name]?.[0];
   }

   useEffect(() => {
       if(id) {
           dispatch(getUser(id))
       }
   }, [id]);

   useEffect(() => {
       if(userEdit) {
           setName(userEdit.name);
           setEmail(userEdit.email);
           setPassword(userEdit.password);
           setPhone(userEdit.phone);
           setRole(userEdit.role);
       }
   }, [userEdit]);

    const handleEditUser = async () => {
        const data = {
            name,
            email,
            password,
            phone,
            role,
        }

        const response = await dispatch(updateUser( {id, data }));
        if(response.payload.status === STATUS_SUCCESS) {
            swal("Successfully!", "You updated user!", "success");
            await dispatch(getAll());
            history.push('/users');
        }
    }

    const theme = createTheme();

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                    <Typography component="h1" variant="h5">
                        Update User Information
                    </Typography>
                    <Box component="form"  noValidate sx={{ mt: 1 }}>
                        <div>
                            <label>Name: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                name="name"
                                autoComplete="name"
                                autoFocus
                                onChange={ event => setName(event.target.value) }
                                value={ name }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`name`)}</span>
                        </div>
                        <div>
                            <label>Email Address: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                onChange={ event => setEmail(event.target.value) }
                                value={ email }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`email`)}</span>
                        </div>

                        <div>
                            <label>Password: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={ event => setPassword(event.target.value) }
                                value={ password }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`password`)}</span>
                        </div>
                        <div>
                            <label>Phone number: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="phone"
                                type="text"
                                id="phone"
                                autoComplete="phone"
                                onChange={ event => setPhone(event.target.value) }
                                value={ phone }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`phone`)}</span>
                        </div>
                        <div>
                            <label>Role: </label>
                            <FormControl fullWidth>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={role}
                                    onChange={ event => setRole(event.target.value) }
                                >
                                    <MenuItem value="Admin">Admin</MenuItem>
                                    <MenuItem value="User">User</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            component={Link}
                            to={'/users'}
                            sx={{ mt: 3, mb: 2 }}
                            disabled={false}
                            onClick={ handleEditUser }
                        >
                            Update User
                        </Button>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    )
}

export default EditUser;