import React from 'react';
import {useEffect, useState} from "react";
import UserList from "./UserList";
import {useDispatch} from "react-redux";
import {getAll} from "../../features/userSlice";
import {IconButton} from "@mui/material";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import {useHistory} from "react-router";
import {useDebounce} from "../../hooks/useDebounce";
import {useAuth} from "../../contexts/authContext";

const User = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const auth = useAuth();

    const [nameSearch, setNameSearch] = useState();

    useEffect(() => {
        dispatch(getAll());
    }, [dispatch]);

    const createUser = () => {
        history.push("/users/create")
    }

    const searchWithDebounce = useDebounce((...params) => {
        let data = {
            name: params?.[0]
        }
        dispatch(getAll(data))
    })

    const searchWithName = (event) => {
        const name = event.target.value;
        setNameSearch(name);
        searchWithDebounce(name)
    }

    return (
        <div>
            <div>
                {auth.authorize.hasPermission("user-create") ? <IconButton aria-label="delete" size="large" >
                    <AddCircleIcon fontSize="large" onClick={createUser}/>
                </IconButton> : ""}
                <input value={nameSearch} onChange={ event => searchWithName(event)}/>
            </div>
            <div>
                <UserList />
            </div>
        </div>

    )
}


export default User;

