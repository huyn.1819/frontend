import React from 'react';
import {
    IconButton, Table, TableBody,
    TableCell, TableContainer, TableHead,
    TableRow,
} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {useDispatch, useSelector} from "react-redux";
import {deleteUser, getAll, getUsers} from "../../features/userSlice";
import Paper from "@mui/material/Paper";
import {useHistory} from "react-router";
import swal from "sweetalert";
import {useAuth} from "../../contexts/authContext";

const UserList = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const users = useSelector(getUsers);

    const auth = useAuth();

    const updateUser = (id) => {
        history.push(`users/update/${id}`);
    }

    const showUser = (id) => {
        history.push(`/users/show/${id}`)
    }

    const destroyUser = async ( id ) => {
            swal({
                title: "Are you sure delete this user?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then(async (willDelete) => {
                    if (willDelete) {
                        await dispatch(deleteUser(id));
                        swal("You deleted user successfully!", {
                            icon: "success",
                        });
                        dispatch(getAll())
                    }
                });

    }

    const renderUsers = users => {
        if (users && users?.data && users?.data?.length > 0){
            return users?.data.map((user, i) => (
            <TableRow
                    key={i}
                    data={user}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                    <TableCell component="th" scope="row" align="center">
                        {i+1}
                    </TableCell>
                    <TableCell align="center">{user.name}</TableCell>
                    <TableCell align="center">{user.email}</TableCell>
                    <TableCell align="center">{user?.roles}</TableCell>
                    <TableCell align="center">
                        <IconButton aria-label="delete" size="large">
                            <VisibilityIcon fontSize="inherit" onClick={ () => showUser(user.id) } />
                        </IconButton>
                        {auth.authorize.hasPermission("user-edit") ? (<IconButton aria-label="delete" size="large">
                            <EditIcon fontSize="inherit" onClick={ () => updateUser(user.id)}/>
                        </IconButton>) : ""}
                        {auth.authorize.hasPermission("user-delete") ? <IconButton aria-label="delete" size="large">
                            <DeleteIcon fontSize="inherit" onClick={ () => destroyUser(user.id) }/>
                        </IconButton> : ""}
                    </TableCell>
                </TableRow>
            ));
        }
    }

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Order</TableCell>
                        <TableCell align="center">Name</TableCell>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">Role</TableCell>
                        <TableCell align="center">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderUsers(users)}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default UserList;