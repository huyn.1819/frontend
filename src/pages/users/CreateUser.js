import React from 'react';
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import Typography from "@mui/material/Typography";
import {ThemeProvider} from "@emotion/react";
import Container from "@mui/material/Container";
import {CssBaseline, FormControl, MenuItem, Select, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {createTheme} from "@mui/material/styles";
import Link from "@mui/material/Link";
import {useHistory} from "react-router";
import {createUser, getAll, getErrors} from "../../features/userSlice";
import swal from "sweetalert";

const STATUS_SUCCESS = 200;

const CreateUser = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const errors = useSelector(getErrors);

    let [name, setName] = useState("");
    let [email, setEmail] = useState("");
    let [password, setPassword] = useState("");
    let [phone, setPhone] = useState("");
    let [role, setRole] = useState("");

    const handleSubmit = async () => {
        const data = {
            name,
            email,
            password,
            phone,
            role
        };

        const response = await dispatch(createUser(data));

        if (response.payload.status === STATUS_SUCCESS) {
            swal("Successfully!", "You created new user!", "success");
            await dispatch(getAll())
            history.push('/users');
        }
    };

    const getError = (name) => {
        return errors?.[name]?.[0];
    }

    const theme = createTheme();

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                <Typography component="h1" variant="h5">
                    Create New User
                </Typography>
                <Box component="form"  noValidate sx={{ mt: 1 }}>
                    <div>
                        <label>Name: </label>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            name="name"
                            autoComplete="name"
                            autoFocus
                            onChange={ event => setName(event.target.value) }
                            value={ name }
                        />
                        <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`name`)}</span>
                    </div>
                    <div>
                        <label>Email Address: </label>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={ event => setEmail(event.target.value) }
                            value={ email }
                        />
                        <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`email`)}</span>
                    </div>

                    <div>
                        <label>Password: </label>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={ event => setPassword(event.target.value) }
                            value={ password }
                        />
                        <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`password`)}</span>
                    </div>
                    <div>
                        <label>Phone number: </label>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="phone"
                            type="text"
                            id="phone"
                            autoComplete="phone"
                            onChange={ event => setPhone(event.target.value) }
                            value={ phone }
                        />
                        <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`phone`)}</span>
                    </div>
                    <div>
                        <label>Role: </label>
                        <FormControl fullWidth>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={role}
                                onChange={ event => setRole(event.target.value) }
                            >
                                <MenuItem value="admin">Admin</MenuItem>
                                <MenuItem value="user">User</MenuItem>
                            </Select>
                        </FormControl>
                        <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`role`)}</span>
                    </div>

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        component={Link}
                        to='/users'
                        sx={{ mt: 3, mb: 2 }}
                        onClick={ handleSubmit }
                    >
                        Create User
                    </Button>
                </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
};

export default CreateUser;