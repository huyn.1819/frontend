import {useDispatch, useSelector} from "react-redux";
import {getDetailUser, getUser} from "../../features/userSlice";
import {useParams} from "react-router";
import React, {useEffect} from "react";
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Paper from "@mui/material/Paper";


const ShowUser = () => {

    const userShow = useSelector(getDetailUser);

    const { id } = useParams();

    const dispatch = useDispatch();

    useEffect(() => {
        if(id) {
            dispatch(getUser(id))
        }
    }, [id]);

    const renderUser = userShow => {
        if(userShow) {
            return (
                <TableRow
                    key={userShow.email}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                    <TableCell align="center">{userShow.name}</TableCell>
                    <TableCell align="center">{userShow.email}</TableCell>
                    <TableCell align="center">{userShow?.roles}</TableCell>
                </TableRow>
            )
        }
    }

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Name</TableCell>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">Role</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderUser(userShow)}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default ShowUser;