import {useDispatch, useSelector} from "react-redux";
import React from "react";
import {deleteRole, getAll, getRoles} from "../../features/roleSlice";
import {
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import Paper from "@mui/material/Paper";
import {useHistory} from "react-router";
import swal from "sweetalert";
import {useAuth} from "../../contexts/authContext";

const STATUS_SUCCESS = 200;

const RoleList = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const auth = useAuth();

    const roles = useSelector(getRoles)

    const updateRole = (id) => {
        history.push(`/roles/update/${id}`)
    }

    const showRole = (id) => {
        history.push(`/roles/show/${id}`)
    }

    const destroyRole = async ( id ) => {
        const response = await dispatch(deleteRole(id));
        if (response.payload.status === STATUS_SUCCESS) {
            swal({
                title: "Are you sure delete this role?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("You deleted role successfully!", {
                            icon: "success",
                        });
                        dispatch(getAll)
                    }
                });
        }
    }

    const renderRoles = roles => {
        if (roles && roles?.data && roles?.data?.length > 0){
            return roles.data.map((role, key) => {
                let currentPage = roles.meta.current_page;
                let perPage = roles.meta.per_page;
                let index = (currentPage - 1) * perPage + key + 1;
                return(
                    <TableRow
                        key={`${role.id} item`}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                        <TableCell component="th" scope="row" align="center">
                            {index}
                        </TableCell>
                        <TableCell align="center">{role.name}</TableCell>
                        <TableCell align="center">{role.permission.map( (per) => {
                            return (
                                <p align="center" key={`${per.id} item`}>
                                    <span align="center" key={per}>{ `${per.name}` }</span>
                                </p>
                            )
                        })}</TableCell>

                        <TableCell align="center">
                            <IconButton aria-label="delete" size="large">
                                <VisibilityIcon fontSize="inherit" onClick={ () => showRole(role.id)}/>
                            </IconButton>
                            {auth.authorize.hasPermission("role-edit") ? <IconButton aria-label="delete" size="large">
                                <EditIcon fontSize="inherit" onClick={ () => updateRole(role.id)}/>
                            </IconButton> : ""}
                            {auth.authorize.hasPermission("role-delete") ? <IconButton aria-label="delete" size="large">
                                <DeleteIcon fontSize="inherit" onClick={ () => destroyRole(role.id) }/>
                            </IconButton> : ""}
                        </TableCell>
                    </TableRow>
                )})
        }
    }

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Order</TableCell>
                        <TableCell align="center">Role</TableCell>
                        <TableCell align="center">Permission</TableCell>
                        <TableCell align="center">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderRoles(roles)}
                </TableBody>
            </Table>
        </TableContainer>

    )
}

export default RoleList;