import React from 'react';
import {useEffect, useState} from "react";
import {getAll, getErrors, getRole, getRoleById, updateRole} from "../../features/roleSlice";
import {useHistory, useParams} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {createTheme} from "@mui/material/styles";
import Container from "@mui/material/Container";
import {
    Checkbox,
    CssBaseline,
    FormControlLabel,
    FormGroup,
    TextField
} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import {ThemeProvider} from "@emotion/react";
import swal from "sweetalert";

const STATUS_SUCCESS = 200;

const EditRole = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    let [name, setName] = useState("");
    let [permission, setPermission] = useState("");

    const roleEdit = useSelector(getRoleById);

    const errors = useSelector(getErrors);

    const { id } = useParams();

    const getError = (name) => {
        return errors?.[name]?.[0];
    }

    useEffect(()=> {
        if(id) {
            dispatch(getRole(id))
        }
    }, [id]);

    useEffect(()=> {
        if(roleEdit) {
            setName(roleEdit.name);
            setPermission(roleEdit.permission);
        }
    }, [roleEdit]);

    const handEdit = async () => {
        const data = {
            name,
            permission,
        }
        const response = await dispatch(updateRole( { id, data }));
        if (response.payload.status === STATUS_SUCCESS) {
            swal("Successfully!", "You updated role!", "success");
            await dispatch(getAll())
            history.push('/roles');
        }
    }

    const theme = createTheme();

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Typography component="h1" variant="h5">
                        Update Role Information
                    </Typography>
                    <Box component="form"  noValidate sx={{ mt: 1 }}>
                        <div>
                            <label>Role: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                name="name"
                                autoComplete="name"
                                autoFocus
                                onChange={ event => setName(event.target.value) }
                                value={ name }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`name`)}</span>
                        </div>

                        <div>
                            <label>Permission: </label>
                            <FormGroup>
                                <FormControlLabel control={<Checkbox value="role-list" name="role-list" onChange={ event => setPermission(event.target.value) }/>} label="Role-list" />
                                <FormControlLabel control={<Checkbox value="role-create" name="role-create" onChange={ event => setPermission(event.target.value) }/>} label="Role-create" />
                                <FormControlLabel control={<Checkbox value="role-edit" name="role-edit" onChange={ event => setPermission(event.target.value) }/>} label="Role-edit" />
                                <FormControlLabel control={<Checkbox value="role-delete" name="role-delete" onChange={ event => setPermission(event.target.value) }/>} label="Role-delete" />
                                <FormControlLabel control={<Checkbox value="user-list" name="user-list" onChange={ event => setPermission(event.target.value) }/>} label="User-list" />
                                <FormControlLabel control={<Checkbox value="user-create" name="user-create" onChange={ event => setPermission(event.target.value) }/>} label="User-create" />
                                <FormControlLabel control={<Checkbox value="user-edit" name="user-edit" onChange={ event => setPermission(event.target.value) }/>} label="User-edit" />
                                <FormControlLabel control={<Checkbox value="user-delete" name="user-delete" onChange={ event => setPermission(event.target.value) }/>} label="User-delete" />
                            </FormGroup>
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`name`)}</span>
                        </div>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            component={Link}
                            to='/users'
                            sx={{ mt: 3, mb: 2 }}
                            onClick={ handEdit }
                        >
                            Update Role
                        </Button>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    )
}

export default EditRole;