import React from 'react';
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createRole, getAll, getErrors} from "../../features/roleSlice";
import {
    Checkbox,
    CssBaseline,
    FormControlLabel,
    FormGroup,
    TextField,
    useTheme
} from "@mui/material";
import {ThemeProvider} from "@emotion/react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import {useHistory} from "react-router";
import swal from "sweetalert";


const STATUS_SUCCESS = 200;

const CreateRole = () => {

    let [name, setName] = useState("");
    let [permission, setPermission] = useState("");

    const dispatch = useDispatch();
    const history = useHistory();

    const errors = useSelector(getErrors)

    const handleSubmit = async () => {
        let data = {
            name,
            permission
        }
        const response = await dispatch(createRole(data));


        if (response.payload.status === STATUS_SUCCESS) {
            swal("Successfully!", "You created new role!", "success");
            await dispatch(getAll())
            history.push('/users');
        }
    }

    const getError = (name) => {
        return errors?.[name]?.[0];
    }

    const theme = useTheme();

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >

                    <Typography component="h1" variant="h5">
                        Create New Role
                    </Typography>
                    <Box component="form"  noValidate sx={{ mt: 1 }}>
                        <div>
                            <label>Role: </label>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                name="name"
                                autoComplete="name"
                                autoFocus
                                onChange={ event => setName(event.target.value) }
                                value={ name }
                            />
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`name`)}</span>

                        </div>

                        <div>
                            <label>Permission: </label>
                            <FormGroup>
                                <FormControlLabel control={<Checkbox value="role-list" name="role-list" onChange={ event => setPermission(event.target.value) }/>} label="Role-list" />
                                <FormControlLabel control={<Checkbox value="role-create" name="role-create" onChange={ event => setPermission(event.target.value) }/>} label="Role-create" />
                                <FormControlLabel control={<Checkbox value="role-edit" name="role-edit" onChange={ event => setPermission(event.target.value) }/>} label="Role-edit" />
                                <FormControlLabel control={<Checkbox value="role-delete" name="role-delete" onChange={ event => setPermission(event.target.value) }/>} label="Role-delete" />
                                <FormControlLabel control={<Checkbox value="user-list" name="user-list" onChange={ event => setPermission(event.target.value) }/>} label="User-list" />
                                <FormControlLabel control={<Checkbox value="user-create" name="user-create" onChange={ event => setPermission(event.target.value) }/>} label="User-create" />
                                <FormControlLabel control={<Checkbox value="user-edit" name="user-edit" onChange={ event => setPermission(event.target.value) }/>} label="User-edit" />
                                <FormControlLabel control={<Checkbox value="user-delete" name="user-delete" onChange={ event => setPermission(event.target.value) }/>} label="User-delete" />
                            </FormGroup>
                            <span className="m-0 errors errors-name" style={{color:"red"}}>{getError(`permission`)}</span>
                        </div>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            component={Link}
                            to='/roles'
                            sx={{ mt: 3, mb: 2 }}
                            onClick={ handleSubmit }
                        >
                            Create Role
                        </Button>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    )
}

export default CreateRole;