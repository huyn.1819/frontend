import React from 'react';
import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {getAll} from "../../features/roleSlice";
import {IconButton} from "@mui/material";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import RoleList from "./RoleList";
import {useHistory} from "react-router";
import {useDebounce} from "../../hooks/useDebounce";
import {useAuth} from "../../contexts/authContext";

const Role = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const auth = useAuth();

    const [nameSearch, setNameSearch] = useState();

    useEffect(() => {
        dispatch(getAll());
        console.log("2")
    }, [dispatch]);

    const createRole = () => {
        history.push('/roles/create');
    }

    const searchWithDebounce = useDebounce((...params)=>{
        let data = {
            name: params?.[0]
        }
        dispatch(getAll(data))
    })

    const searchWithName = (event) => {
        const name = event.target.value;
        setNameSearch(name);
        searchWithDebounce(name)
    }

    return (
        <div>
            <div>
                <div>
                    {auth.authorize.hasPermission("role-create") ? <IconButton aria-label="delete" size="large" >
                        <AddCircleIcon fontSize="large" onClick={createRole}/>
                    </IconButton> : ""}
                    <input value={nameSearch} onChange={event => searchWithName(event)}/>
                </div>
                <div>
                    <RoleList />
                </div>
            </div>
        </div>
    )
}

export default Role;