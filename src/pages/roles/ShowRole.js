import {useDispatch, useSelector} from "react-redux";
import {getRole, getRoleById} from "../../features/roleSlice";
import {useParams} from "react-router";
import React, {useEffect} from "react";
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Paper from "@mui/material/Paper";


const ShowRole = () => {

    const roleShow = useSelector(getRoleById);

    const { id } = useParams();

    const dispatch = useDispatch();

    useEffect(()=> {
        if(id) {
            dispatch(getRole(id))
        }
    }, [id]);

    const renderRole = roleShow => {
        if (roleShow){
           return (
                   <TableRow
                        key={roleShow.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                   >
                       <TableCell align="center">{roleShow.name}</TableCell>
                       {/*<TableCell align="center">{roleShow.permission}</TableCell>*/}
                       <TableCell align="center">{roleShow?.permission?.map( (per) => {
                           return (
                               <p align="center" key={`${per.id} item`}>
                                   <span key={per}>{ `${per.name}`}</span>
                               </p>
                           )
                       })}</TableCell>
                   </TableRow>
           )
        }
    }

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Role</TableCell>
                        <TableCell align="center">Permission</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderRole(roleShow)}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default ShowRole;
